#!/usr/bin/env python3
# coding=UTF-8
#==============================================================================
# title           :tetris.py
# description     :Tetris
# author          :Olivier Meloche
# credit          :silvasur from GitHub
# date            :20200216
# version         :0.0.1
# usage           :python main.py
# python_version  :3.7.1
#==============================================================================
from random import randrange as rand
import pygame, sys
import numpy as np
from rounded_rect import AAfilledRoundedRect

cell_size =	32                  #px
cols = 10                       #grid y
rows = 20                       #grid x
fps = 60                        #framerate
volume = 1                      #audio
rows += 2                       #top invisible rows
width = cell_size*(cols+16)     #screen width
height = cell_size*(rows+2)     #screen height
right_limit = cell_size*cols    #right section of the screen

colors = [(0x0),        #black
          (0x990099),   #purple
          (0x00ff00),   #green
          (0xff0000),   #red
          (0x0000ff),   #blue
          (0xff8000),   #orange
          (0x00ffff),   #cyan
          (0xffff00),   #yellow
          (0x080808),   #dark_gray
          (0xffffff)]   #white

tetromino = {
  1:[[1, 1, 1],
     [0, 1, 0]],

  2:[[0, 2, 2],
     [2, 2, 0]],

  3:[[3, 3, 0],
     [0, 3, 3]],

  4:[[4, 0, 0],
     [4, 4, 4]],

  5:[[0, 0, 5],
     [5, 5, 5]],

  6:[[6, 6, 6, 6]],

  7:[[7, 7],
     [7, 7]] }
"""
tetromino = {1:[[0,1,0],
                [1,1,1],
                [0,0,0]],

             2:[[0,2,2],
                [2,2,0],
                [0,0,0]],

             3:[[3,3,0],
                [0,3,3],
                [0,0,0]],

             4:[[4,0,0],
                [4,4,4],
                [0,0,0]],

             5:[[0,0,5],
                [5,5,5],
                [0,0,0]],

             6:[[0,0,0,0],
                [6,6,6,6],
                [0,0,0,0],
                [0,0,0,0]],

             7:[[7,7],
                [7,7]]}
"""

def rotate_clockwise(shape):
    shape = np.rot90(shape, 3)
    #print(shape)
    return shape

#def rotate_clockwise(shape):
#    return [ [ shape[y][x]
#        for y in range(len(shape)) ]
#            for x in range(len(shape[0]) - 1, -1, -1) ]

def is_collision(board, shape, offset):
    off_x, off_y = offset
    for cy, row in enumerate(shape):
        for cx, cell in enumerate(row):
            try:
                if cell and board[ cy + off_y ][ cx + off_x ]:
                    return True
            except IndexError:
                return True
    return False

def remove_row(board, row):
    del board[row]
    return [[0 for i in range(cols)]] + board

def join_matrixes(mat1, mat2, mat2_off):
    off_x, off_y = mat2_off
    for cy, row in enumerate(mat2):
        for cx, val in enumerate(row):
            mat1[cy+off_y-1][cx+off_x] += val
    return mat1

def new_board():
    board = [[0 for x in range(cols)] for y in range(rows)]
    #board += [[ 1 for x in range(cols)]]
    floor = []
    for x in range(cols):
        floor.append(1)
    board.append(floor)
    #print(board)
    return board

class Tetris(object):
    def __init__(self):
        self.width = width
        self.height = height
        self.bground_grid = [[ 8 if x%2==y%2 else 0 for x in range(cols)] for y in range(rows)]
        self.font = font
        self.screen = screen
        self.top_left_board = ((self.width/2)-(cell_size*cols)/2)
        self.right_limit = (self.top_left_board + cell_size*cols)
        self.init_game()

    def generate_first_bag(self):
        first_piece = [1,4,5,6]
        piece = [1,2,3,4,5,6,7]
        self.bag.append(first_piece[rand(len(first_piece))])
        piece.remove(self.bag[0])
        while len(piece) > 0:
            self.bag.append(piece.pop(rand(len(piece))))
        print("First bag generated "+str(len(self.bag))+" tetrominoes "+ str(self.bag))

    def generate_bag(self):
        piece = [1,2,3,4,5,6,7]
        while len(piece) > 0:
            self.bag.append(piece.pop(rand(len(piece))))
        print("Bag generated "+str(len(self.bag))+" tetrominoes "+ str(self.bag))

    def pick_from_bag(self):
        try:
            return self.bag.pop(0)
        except:
            self.generate_bag()
            return self.bag.pop(0)

    def new_stone(self, *id):
        if id:
            self.holded = id
            self.stone = np.array(tetromino[id])
        else:
            self.stone = self.next_stone[:]
            self.next_stone = np.array(tetromino[self.pick_from_bag()])
        self.stone_x = int(cols / 2 - len(self.stone[0])/2)
        self.stone_y = 2 #était à 0 avant
        #if is_collision(self.board, self.stone, (self.stone_x, self.stone_y)):
        #    self.gameover = True
        ###  V  V  V  V  V  V  V  V #### NOUVEAU
        if is_collision(self.board, self.stone, (self.stone_x, self.stone_y)):
            self.stone_y = 0
            if is_collision(self.board, self.stone, (self.stone_x, self.stone_y)):
                self.gameover = True

    def init_game(self):
        self.bag = []
        self.generate_first_bag()
        self.id = self.pick_from_bag()
        self.next_stone = np.array(tetromino[self.id])
        self.newdelay = 1000
        self.board = new_board()
        self.new_stone()
        self.level = 1
        self.score = 0
        self.lines = 0
        self.total_played = 0
        self.holded = 0
        self.can_hold = True
        self.timer = pygame.time.get_ticks()
        pygame.time.set_timer(pygame.USEREVENT+1, self.newdelay) #drop speed

    def show_msg(self, msg, coord):
        x,y = coord
        for line in msg.splitlines():
            self.screen.blit(self.font.render(line, False, (255,255,255), (0,0,0)), (x,y))
            y+=14

    def center_msg(self, msg):
        for i, line in enumerate(msg.splitlines()):
            msg_image =  self.font.render(line, False, (255,255,255), (0,0,0))

            msgim_center_x, msgim_center_y = msg_image.get_size()
            msgim_center_x //= 2
            msgim_center_y //= 2

            self.screen.blit(msg_image, (self.width // 2-msgim_center_x, self.height // 2-msgim_center_y+i*22))

    def draw_matrix(self, matrix, offset):
        off_x, off_y  = offset
        for y, row in enumerate(matrix):
            for x, val in enumerate(row):
                if val:
                    pygame.draw.rect(self.screen, colors[val], pygame.Rect(
            (off_x+x)*cell_size+self.top_left_board, (off_y+y)*cell_size, cell_size, cell_size), 0)
                    pygame.draw.rect(self.screen, colors[0], pygame.Rect(
            (off_x+x)*cell_size+self.top_left_board, (off_y+y)*cell_size, cell_size, cell_size), 2)

    def draw_blackbox(self):
        pygame.draw.rect(self.screen, colors[0], ((self.top_left_board, 0),
                                (cols*cell_size, cell_size*2)), 0)
        pygame.draw.rect(self.screen, colors[0], ((self.top_left_board, self.height-cell_size*2),
                                (cols*cell_size, cell_size*2)), 0)

    def add_cl_lines(self, n):
        linescores = [0, 40, 100, 300, 1200]
        self.lines += n
        self.score += linescores[n] * self.level
        if self.lines >= self.level*6:
            self.level += 1
            self.newdelay = 1000-50*(self.level-1)
            self.newdelay = 100 if self.newdelay < 100 else self.newdelay
            pygame.time.set_timer(pygame.USEREVENT+1, self.newdelay)

    def move(self, delta_x):
        if not self.gameover and not self.paused:
            new_x = self.stone_x + delta_x
            if new_x < 0:
                new_x = 0
            if new_x > cols - len(self.stone[0]):
                new_x = cols - len(self.stone[0])
            if not is_collision(self.board, self.stone, (new_x, self.stone_y)):
                self.stone_x = new_x

    def quit(self):
        sys.exit()

    def drop(self, manual):
        if not self.gameover and not self.paused:
            self.score += 1 if manual else 0
            self.stone_y += 1
            if is_collision(self.board, self.stone, (self.stone_x, self.stone_y)):
                pygame.time.set_timer(pygame.USEREVENT+1, self.newdelay) # test
                self.board = join_matrixes(self.board, self.stone, (self.stone_x, self.stone_y))
                self.new_stone()
                self.total_played += 1
                cleared_rows = 0
                while True:
                    for i, row in enumerate(self.board[:-1]):
                        if 0 not in row:
                            self.board = remove_row(self.board, i)
                            cleared_rows += 1
                            break
                    else:
                        break
                self.add_cl_lines(cleared_rows)
                return True
        return False

    def insta_drop(self):
        if not self.gameover and not self.paused:
            while(not self.drop(True)):
                pass

    def rotate_stone(self):
        if not self.gameover and not self.paused:
            new_stone = rotate_clockwise(self.stone)
            #print(new_stone)
            if not is_collision(self.board, new_stone, (self.stone_x, self.stone_y)):
                self.stone = new_stone

    def toggle_pause(self):
        self.paused = not self.paused

    def start_game(self):
        if self.gameover:
            self.init_game()
            self.gameover = False

    def get_tpm(self):
        try:
            return round(self.total_played / (pygame.time.get_ticks()-self.timer) * 60000)
        except ZeroDivisionError:
            return 0

    def hold(self):
        if not self.holded:
            self.id = self.holded
            self.new_stone()
        else:
            self.new_stone(self.id)


###############################################################################
class Menu(object):
    def __init__(self):
        pass

###############################################################################
def engine():
    tetris.gameover = False
    tetris.paused = False
    clock = pygame.time.Clock()
    horizontal_delay = 0
    vertical_delay = 0

    while True:
        tetris.screen.fill((0,0,0))
        if tetris.gameover:
            tetris.center_msg("""Game Over!\nScore: %d\n\nPress ENTER to back to menu""" % tetris.score)
        elif tetris.paused:
            tetris.center_msg("Paused")
        else:
            tetris.show_msg("Next Tetromino", (tetris.right_limit+cell_size, 30))
            tetris.show_msg("Score %d\n\n\nLevel %d\n\nLines %d" % (tetris.score, tetris.level, tetris.lines), (tetris.right_limit+cell_size, cell_size*5))
            tetris.show_msg("TPM %d" % (tetris.get_tpm()), (tetris.right_limit+cell_size, cell_size*8))
            tetris.draw_matrix(tetris.bground_grid, (0,0))
            tetris.draw_matrix(tetris.board, (0,0))
            tetris.draw_matrix(tetris.stone, (tetris.stone_x, tetris.stone_y))
            tetris.draw_matrix(tetris.next_stone, (cols+1,2))
            tetris.draw_blackbox()
            pygame.draw.rect(tetris.screen, colors[9], ((tetris.top_left_board, cell_size*2), (tetris.right_limit/2+cell_size, tetris.height-cell_size*4)), 1)
            #HOLD INTERFACE
            tetris.show_msg("Hold", (cell_size*2, cell_size))
            AAfilledRoundedRect(tetris.screen ,(cell_size*2, cell_size*2 ,cell_size*4,cell_size*4), (45,45,45) ,0.08)
            if tetris.holded:
                tetris.draw_matrix(tetromino[tetris.holded], (-6,3))
        pygame.display.update()

        keyState = pygame.key.get_pressed()
        if keyState[pygame.K_DOWN]:
            vertical_delay += 1
            if vertical_delay >= 3:
                tetris.drop(True)
                vertical_delay = 0
        if keyState[pygame.K_LEFT]:
            if horizontal_delay == 0:
                tetris.move(-1)
            if horizontal_delay == 15:
                tetris.move(-1)
                horizontal_delay = 12
            horizontal_delay += 1
        elif keyState[pygame.K_RIGHT]:
            if horizontal_delay == 0:
                tetris.move(+1)
            if horizontal_delay == 15:
                tetris.move(+1)
                horizontal_delay = 12
            horizontal_delay += 1

        for event in pygame.event.get():
            if event.type == pygame.USEREVENT+1:
                tetris.drop(False)
            if event.type == pygame.QUIT:
                tetris.quit()

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    horizontal_delay = 0
                elif event.key == pygame.K_RIGHT:
                    horizontal_delay = 0

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    tetris.quit()
                elif event.key == pygame.K_UP:
                    tetris.rotate_stone()
                elif event.key == pygame.K_p:
                    tetris.toggle_pause()
                elif event.key == pygame.K_RETURN:
                    tetris.start_game()
                elif event.key == pygame.K_SPACE:
                    tetris.insta_drop()
                elif event.key == pygame.K_TAB:
                    tetris.hold()

        clock.tick(fps)

###############################################################################
if __name__ == '__main__':
    version = "0.0.1"
    pygame.init()
    font = pygame.font.Font("sprites/font.ttf", 12)
    screen = pygame.display.set_mode((width, height))
    #pygame.key.set_repeat(250,25) #keyboard repeat delay
    pygame.event.set_blocked(pygame.MOUSEMOTION) #disable mouse movement events
    pygame.mouse.set_visible(0) #hide mouse
    pygame.display.set_caption("Tetris v"+version)
    pygame.display.set_icon(pygame.image.load("sprites/icon.png"))
    pygame.mixer.music.set_volume(volume)

    print("Initializing components")
    tetris = Tetris()
    print("Initializing game...")
    engine()


  ########################
  #IDÉE:
  #DONE grid center screen
  #DONE sac
  #DONE ajouter 2 ligne en haut (invisible)
  #DONE tpm (tetris per min)
  #hold
  #tetris shadow tout en bas (utiliser fonction insta_drop)
  #SEMI refaire rotation
  #menu
  #highest score ever
  #goal --> line left = level up
  #Stats for each tetromino
  #Stats for single/double/triple/tetris/back2back tetris
  #gfx effect
  #audio
  #lvl 10+ last song
  ##########################
